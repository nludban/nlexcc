/*********************************************************************/
/*
 * example-0.c++
 */
/*********************************************************************/

typedef nlexcc::YY<MyContext> YY;

class LexRules : public nlexcc::LexRules
{
public:

   token<"INTEGER", int>	INTEGER;
   token<"FLOAT", float>	FLOAT;
   keyword<"PRINT">		PRINT;
   punct<"+">			ADD;
   punct<"*">			MUL;
   punct<"(">			LPAREN;
   punct<")">			RPAREN;

   virtual ~LexRules();

   LexRules()
   {
      INTEGER.configure(this)
	 .pattern(R"/\d+/")
	 .action(
	    [&] (YY &yy) {
	    });

      FLOAT.configure(this)
	 .pattern(R"/\d+\.\d+/")
	 .pattern(R"/\d+\.\d+[eE][-+]+\d+/")
	 .action(
	    [&] (YY &yy) {
	    });

      ADD.configure(this)
	 .literal("+");

   }
};

//--------------------------------------------------
class Symbol
{
};

class TerminalBase : public Symbol
{
};

template <typename T>
class Terminal<T> : public TerminalBase
{
};

class NonTerminalBase : public Symbol
{
};

template <typename T>
class NonTerminal<T> : public NonTerminalBase
{
};

class SymbolFactory
{
};

class TerminalFactory : public SymbolFactory
{
};

class NonTerminalFactory : public SymbolFactory
{
};

template <typename LexRules>
class nlexcc::Grammar<LexRules>
{
   template <typename T>
   class symbol<T> : public NonTerminalFactory<T>
   {
   };
};

class Stack
{
   
};

//--------------------------------------------------

class Grammar : public nlexcc::Grammar<LexRules>
{
   symbol<Expression>		expr;
   symbol<ConstantExpression>	const_expr;
   symbol<BinaryExpression>	bin_expr;
   symbol<Statement>		stmt;
   symbol<PrintStatement>	print_stmt;

   virtual ~Grammar();

   Grammar()
   {
      const_expr.configure(
	 this,
	 [&] () {

	    auto &r1 = const_expr.rule<INTEGER>();
	    r1.action([&] (r1::S &s) {
		  return new ConstantExpression(s(1));
	       });

	    auto &r2 = const_expr.rule<FLOAT>();
	    r2.action([&] (r2::S &s) {
		  return new ConstantExpression(s(1));
	       });
	 });

      //this->start();
   }
};

//http://eli.thegreenplace.net/2014/variadic-templates-in-c/
// struct tuple; get<n>...
//
// typedef tuple<A, B, C> S;
// s(n) = T s.value if n==1 else s.next(n-1)

/**/
