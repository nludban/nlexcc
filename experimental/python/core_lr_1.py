#!/usr/local/bin/python3.5
## core_lr_1.py -- LR(1) algorithms.

from pprint import pprint

import pylexcc
import pylex
import pycc

from chillset import ChillSet
from core_lr import Production, Item

_P_NOTICE = True	# Print table generation progress and summary
_P_INFO = True		# Dump compiled tables
_P_DEBUG = True		# Internal diagnostics


def _mktables_LR_1(lexer, grammar):
    # Compilers Principles, Techniques, and Tools
    # Fig 4.38, pg 232
    #
    # Conventions (as seen in literature):
    #	terminal = lowercase
    #	non-terminal = uppercase
    #	string of term and/or non = greek
    #	sets = uppercase

    G = grammar.productions()
    if _P_DEBUG:
        print('G=%r' % G)
    START = grammar.start_symbol()
    EPSILON = grammar.epsilon_symbol()
    EPSILON_SET = ChillSet([ EPSILON, ]).freeze()
    #print(('EPS=%r' % EPSILON))
    #print(('EPS=%s' % EPSILON).encode('utf-8'))
    EOF = lexer.eof()

    if _P_INFO:
        print('Grammar production rules:')
        for pr in G.iter_sorted():
            print('\t%s' % pr)

    def greekify(x):
        # Canonical form of tuple of symbols, making the empty
        # string (epsilon) go away or appear as needed...
        x = [ s for s in x if s != EPSILON ]
        if not x:
            x.append(EPSILON)
        return tuple(x)

    _first = { }
    def _precompute_firsts():
        # Page 189:
        first = _first

        # 1. If X is terminal, then FIRST(X) is {X}
        for X in lexer.terminals():
            assert isinstance(X, pylex.TerminalFactory), repr(X)
            first[X] = ChillSet([ X, ])
        first[EOF] = ChillSet([ EOF, ])		# ???
        first[EPSILON] = ChillSet([ EPSILON, ])	# ???

        # 2. If X -> EPSILON is a production, then add
        #	EPSIOLON to FIRST(X)
        for pr in G.iter_sorted():
            first[pr.nonterminal] = ChillSet()
        for pr in G.iter_sorted():
            if pr.symbols[0] is EPSILON:
                first[pr.nonterminal].add(EPSILON)

        # 3. For all non-terminal productions X -> Y1 Y2 Y3 ...
        #	For all w except EPSILON in FIRST(Y1)
        #		Add w to FIRST(X)
        #	If EPSILON in FIRST(Y1)
        #		For all w ... Y2
        #			Add w to FIRST(X)
        #		...
        #	If EPSILON in all FIRST(Yi)
        #		Add EPSILON to FIRST(X)
        while True:
            for f in _first.values():
                f.chill()

            for pr in G.iter_sorted():
                X = pr.nonterminal
                Y = pr.symbols
                nf = ChillSet()
                for i in range(len(Y)):
                    assert isinstance(Y[i], pylexcc.SymbolFactory)
                    if EPSILON not in first[Y[i]]:
                        nf.update(first[Y[i]])
                        break
                    nf.update(first[Y[i]] - EPSILON_SET)
                else:
                    nf.add(EPSILON)
                first[X].update(nf)

            if any(f.changes() for f in first.values()):
                if _P_DEBUG:
                    for X, f in first.items():
                        ch = f.changes()
                        if ch:
                            print('Changed: %s += %s' % ( X, ch ))
                continue
            break

        # 4. Dump for diagnostics.
        if _P_INFO:
            print('Firsts:')
            for key, value in sorted(first.items()):
                #print('\t%r : %r' % (key,value))
                print('\t%s : %s' % (
                    key,
                    ' '.join([ str(t) for t in value.iter_sorted() ],
                    )))
        return
    _precompute_firsts()
    grammar_symbols = sorted(_first.keys())

    def first(*X):
        # Page 189:
        #
        # Add all non-EPSILON first(X1)
        # If EPSILON in first(X1)
        #	Add non-EPSILON first(X2)
        #	If EPSILON in first(x2)
        #		Add non-EPSILON first(X3)
        #		...
        # If EPSILON in all(first(X_i))
        #	Add EPSILON
        f = ChillSet()
        for Xi in X:
            assert not isinstance(Xi, tuple), 'Programmer error'
            assert isinstance(Xi, pylexcc.SymbolFactory), repr(Xi)
            if EPSILON not in _first[Xi]:
                f.update(_first[Xi])
                break
            f.update(_first[Xi] - EPSILON_SET)
        else:
            f.add(EPSILON)
        return f


    _follow = { }
    def _precompute_follows():
        # Page 189:
        #
        # 1. Place $ in FOLLOW(S)
        #
        # 2. For each non-terminal A,
        #	For each production A -> alpha B beta,
        #		Add FIRST(beta) to FOLLOW(B), except EPSILON
        # 3.	If there is a production A -> alpha B
        #		Add FOLLOW(A) to FOLLOW(B)
        #	If there is a production A -> alpha B beta
        #		-and- EPSILON in FIRST(beta),
        #		Add FOLLOW(A) to FOLLOW(B)
        # Repeat until no changes.
        follow = _follow

        # Create all follow sets, initially empty.
        for pr in G.iter_sorted():
            follow[pr.nonterminal] = ChillSet()

        # (1) For all S' -> S, add $ to FOLLOW(S)
        for pr in G.iter_sorted():
            if pr.nonterminal == START:
                follow[pr.symbols[0]].add(EOF)

        while True:
            for f in follow.values():
                f.chill()

            for pr in G.iter_sorted():
                A = pr.nonterminal

                # (2)
                for i in range(len(pr.symbols) - 1):
                    B = pr.symbols[i]
                    if not isinstance(B, pycc.NonTerminalFactory):
                        continue
                    beta = pr.symbols[i + 1]
                    follow[B].update(first(beta) - EPSILON_SET)

                # (3.a)
                B = pr.symbols[-1]
                if isinstance(B, pycc.NonTerminalFactory):
                    follow[B].update(follow[A])

                # (3.b)
                if len(pr.symbols) >= 2:
                    B = pr.symbols[-2]
                    beta = pr.symbols[-1]
                    if (isinstance(B, pycc.NonTerminalFactory)
                        and EPSILON in first(beta)):
                        follow[B].update(follow[A])

            if any(f.changes() for f in follow.values()):
                if _P_DEBUG:
                    for X, f in follow.items():
                        ch = f.changes()
                        if ch:
                            print('Changed: %s += %s' % ( X, ch ))
                continue
            break

        for f in follow.values():
            f.freeze()

        # 4. Dump for diagnostics.
        if _P_INFO:
            print('Follows:')
            for key, value in sorted(follow.items()):
                print('\t%s : %s' % (
                    key,
                    ' '.join([ str(t) for t in value.iter_sorted() ],
                    )))
        return
    _precompute_follows()

    def closure(I):
        # Fig 4.38, Page 232.
        assert isinstance(I, ChillSet)
        assert all([ isinstance(item, Item)
                     for item in I.iter_sorted() ]), repr(I)
        I = ChillSet(I) # Copy.

        while True:
            I.chill()

            for item in I.iter_inserted():
#                if _P_DEBUG:
#                    print('closure working on %s...' % item)
                # [ A -> alpha . B beta, a ]
                # XXX alpha and/or beta may be empty
                A = item.nonterminal
                alpha = item.ldot # EPSILON? (not used)
                Bbeta = item.rdot
                if not Bbeta:
                    # XXX reduce?
                    continue
                B = Bbeta[0]
                beta = Bbeta[1:] or (EPSILON,) # XXX
                a = item.lookahead

                if _P_DEBUG and False:
                    print(
                        '[ A=%s -> alpha=(%s) . B=%s beta=(%s), a=%s ]'
                        % ( A,
                            ' '.join([ str(q) for q in alpha ]),
                            B,
                            ' '.join([ str(q) for q in beta ]),
                            a ))

                for pr in G.iter_sorted():
                    # B -> gamma
                    if pr.nonterminal != B:
                        continue
                    gamma = pr.symbols

                    for b in first(*beta, a).iter_sorted(): #?

                        # [ B -> . gamma, b ]
                        ni = Item(B, (), gamma, b)
                        #if ni not in I:
                        I.add(ni)
            # Until no more items can be added to I.
            if I.changes():
                continue
            break

        #I.freeze()
        return I

    def goto(I, X):
        # Fig 4.38, Page 232.
        J = ChillSet()
        for item in I.iter_inserted():
            if item.rdot and item.rdot[0] == X:
                # For [ A -> alpha . X beta, a ] in I,
                # add [ A -> alpha X . beta, a ] to J.
                J.add(Item(item.nonterminal,
                           item.ldot + (X,),
                           item.rdot[1:],
                           item.lookahead))
        #J.freeze()
        return closure(J)

    def sets_of_items():
        # Fig 4.38, Page 232.
        if _P_NOTICE:
            print('Calculating sets of items...')

        # C := {closure({[S' -> . S, $]})};
        C = ChillSet()	# A set of sets of Items.

        I = ChillSet()
        for pr in G.iter_sorted():
            if pr.nonterminal.name != pycc.START_NAME:
                continue
            # Filtered so pr matches rules of the form:
            #	S' -> S
            # where S is one of the non-terminals passed to g.start().
            I.add(Item(START,		# S' <-
                       ( ),		# () .
                       pr.symbols,	# (S,)
                       EOF))		# lookhead
        if _P_DEBUG:
            print('Initial rule: I=%s' % I)
        I0 = closure(I).freeze()
        if _P_DEBUG:
            print('closure(I)=', str(I0).replace('], [', '],\n\t['))
        C.add(I0)

        while True:
            C.chill()

            # For each set of items I in C,
            # and each grammar symbol X
            # where goto(I, X) is not empty:
            #	Add goto(I, X) to C (if not already)
            for I in C.iter_inserted():
                for X in grammar_symbols:
                    J = goto(I, X).freeze()
                    if J and J not in C:
                        #print('soi: adding %s' % J)
                        C.add(J)

            # Until no more sets of items can be added to C.
            if C.changes():
                if _P_INFO:
                    print('Updated sets of items:')
                    for i, I in enumerate(C.iter_inserted()):
                        print('-%i-' % i, end='')
                        for row in I.iter_inserted():
                            print('\t%s' % row)
                continue
            break

        return I0, C.freeze()

    # Algorithm 4.10, Page 234.
    # XXX Swich for inserted vs canonical order?
    #
    # 1. Construct C = {I0, I1, ... In}, the collection of
    #    sets of items.
    I0, C = sets_of_items()
    if _P_INFO:
        print('Sets of items:')
        for i, I in enumerate(C.iter_inserted()):
            print('-%i-' % i, end='')
            for row in I.iter_inserted():
                print('\t%s' % row)

    # 2. State i of the parser is constructed from Ii.
    states = tuple([ tuple([ str(item) for item in I.iter_inserted() ])
                     for I in C.iter_inserted() ])

    rules = tuple(G.iter_inserted())

    # actions[k] goes with states[k]
    actions = [ { } for k in states ]

    for i, Ii in enumerate(C.iter_inserted()):

        for item in Ii.iter_inserted():

            # 2-a. If [A -> alpha . a beta, b ] is in Ii
            #      and goto(Ii, a) = Ij,
            #      then action[i][a] = shift j.
            if item.rdot:
                a = item.rdot[0]
                if isinstance(a, pylex.TerminalFactory):
                    J = goto(Ii, a).freeze()
                    j = C.find_inserted(J)
                    if j >= 0:
                        actions[i][a] = 'shift %i' % j

            # 2-b. If [A -> alpha ., a] is in Ii, A != S',
            #      then action[i][a] = reduce A -> alpha.
            else: # not item.rdot

                if item.nonterminal != START:
                    pr = Production(item.nonterminal,
                                    item.ldot,
                                    None)
                    k = G.find_inserted(pr)
                    assert k >= 0, pr
                    actions[i][item.lookahead] = ('reduce %i' % k)


            # 2-c. If [S' -> S ., $] is in Ii,
            #      then action[i][$] = accept.
                else: # item.nonterminal == START
                    actions[i][EOF] = 'accept'

    pprint(actions)
    actions = tuple([ tuple(sorted(a.items()))
                      for a in actions ])

    # 3. If goto(Ii, A) = Ij, then goto[i, A] = j ???
    #
    #    // When the reduction removes N symbols and replaces it
    #    // with one (ie, A), N states were also discarded.
    #    // The parser pushes goto(Ii, A) where i is the top-most
    #    // not-discarded state.
    #
    gotos = [ { } for k in states ]
    for i, Ii in enumerate(C.iter_inserted()):

        # Only need to calculate once per next non-terminal
        A_set = set()
        for item in Ii.iter_sorted():
            if item.rdot:
                A = item.rdot[0]
                if isinstance(A, pycc.NonTerminalFactory):
                    A_set.add(A)

        for A in sorted(A_set):
            Ij = goto(Ii, A).freeze()
            j = C.find_inserted(Ij)
            #assert j >= 0, Ij
            if j < 0:
                print('Fail goto:')
                print('\titem = %r' % item)
                print('\tIi = %r' % Ii)
                print('\trule = %s' % pr)
                print('\tIj = %r' % Ij)
            else:
                gotos[i][A] = j

    gotos = tuple([ tuple(sorted(g.items()))
                    for g in gotos ])

    # 4. All entries not defined by (2) and (3) are error.

    # 5. Initial state is the one constructed from [S' -> . S, $].
    state_0 = C.find_inserted(I0)


    return locals()

#---------------------------------------------------------------------#

def mktables_LR_1(lexer, grammar):
    m = _mktables_LR_1(lexer, grammar)

    print('Rules:')
    for n, pr in enumerate(m['rules']):
        print('\t%3i =\t%r' % ( n, pr ))

    print('States:')
    for n, I in enumerate(m['states']):
        print('\t%3i =\t%s' % ( n, '\n\t\t'.join(I) ))

    print('Actions:')
    for n, a in enumerate(m['actions']):
        print('\t%3i =\t%s' % ( n, '\n\t\t'.join([ '%s => %s' % kv
                                                   for kv in a ]) ))

    print('Gotos:')
    for n, g in enumerate(m['gotos']):
        print('\t%3i =\t%s' % ( n, '\n\t\t'.join([ '%s => %s' % kv
                                                   for kv in g ]) ))

    return ( m['rules'],
             m['states'],
             m['state_0'],
             m['actions'],
             m['gotos'] )

#--#
