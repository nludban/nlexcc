#!/usr/local/bin/python3.5
## pylex_re.py

import heapq #re.search -> priority queue
import re
import sys

import pylex
import pylexcc


#class TokenFactory(pylex.TokenFactory):
#    pass
#
#token_impl = '''
#class {typename:s}(TokenFactory):
#
#    def __init__(self): pass
#
#    def __call__(self, {union:s}):
#        return self._create({union:s})
#'''
        # See namedtuple implementation...
#        typename = '{}_TokenFactory'.format(name)
#        namespace = dict(
#            __name__	= 'token_{}'.format(name),
#            TokenFactory = TokenFactory)
#        impl = token_impl.format(
#            typename	= typename,
#            union	= union)
#        exec(impl, namespace)
#        c = namespace[typename]
#        c._source = impl
#        c.__module__ = sys._getframe(1).f_globals.get('__name__',
#                                                      '__main__')
#        return c()


class LiteralHandler:

    def __init__(self, factory, literal, action):
        assert isinstance(factory, pylex.TerminalFactory), repr(factory)
        self._factory = factory
        self._literal = literal
        if action is None:
            self._action = lambda yy: None
        else:
            self._action = action
        return

    def search(self, text, a, b, at_bol):
        a = text.find(self._literal, a, b)
        return ( -1, 0 ) if a == -1 else ( a, a+len(self._literal) )

    def consume(self, yy):
        return self._action(yy)

    def dumps(self):
        return "%s := ``%s''  # %s" % (
            self._factory,
            self._literal,
            pylexcc._action_repr(self._action),
        )


class PatternHandler:

    def __init__(self, factory, pattern, flags, action):
        assert isinstance(factory, pylex.TerminalFactory), repr(factory)
        self._factory = factory
        self._pattern = pattern # XXX special '^' ?
        if action is None:
            self._action = lambda yy: None
        else:
            self._action = action
        self._caret = pattern.startswith('^')
        self._regex = re.compile(pattern.lstrip('^'),
                                 flags|re.MULTILINE)
        return

    def _carets(self, text, a, b):
        n = a
        while n < b:
            n = text.find('\n', n, b)
            if n == -1:
                break
            n += 1
            yield n
        return

    def search(self, text, a, b, at_bol):
        if self._caret:
            if at_bol:
                m = self._regex.match(text, a, b)
            if not m:
                for n in self._carets(text, a, b):
                    m = self._regex.match(text, n, b)
                    if m:
                        break
        else:
            m = self._regex.search(text, a, b)
        return ( -1, 0 ) if m is None else ( m.start(), m.end() )

    def consume(self, yy):
        return self._action(yy)

    def dumps(self):
        return '%s := /%s/  # %s' % (
            self._factory,
            self._pattern,
            pylexcc._action_repr(self._action),
        )


class UnmatchedHandler:

    def __init__(self): #, action):
        #self._action = action
        return

    def consume(self, yy):
        #if self._action is None:
        #return self._action(yy)
        return yy.unmatched()


class LexerFactory(pylex.LexerFactory):

    def __init__(self):
        pylex.LexerFactory.__init__(self)
        self._patterns = [ ]
        return

    def _impl_terminals(self):
        assert all([ isinstance(ph._factory, pylex.TerminalFactory)
                     for ph in self._patterns ])
        return [ ph._factory for ph in self._patterns ]

    def dumps(self):
        return '\n'.join([ ph.dumps() for ph in self._patterns ])

    def _impl_pattern(self, pattern, flags, factory):
        # From this example:
        #
        # with l.token() as INTEGER:
        #   @INTEGER.pattern(r'\d+')
        #   def action(yy):
        #     return INTEGER(int(yy.text))
        #
        # The factory is a TokenFactory and is not obviously needed
        # here becasue the action already knows it.  Future: make
        # the decorator type-check the return value of the action,
        # it MUST be what the factory produces.
        def deco(action):
            self._patterns.append(PatternHandler(
                factory, pattern, flags, action))
            return action
        return deco

    def _impl_literal(self, literal, factory):
        def deco(action):
            self._patterns.append(LiteralHandler(
                factory, literal, action))
            return action
        return deco

    def _impl_preprocess(self):#, unmatched_action):
        self._unmatched_handler = UnmatchedHandler()#unmatched_action)
        if True:
            print('Patterns:')
            d = [ p.dumps() for p in self._patterns ]
            for p in sorted(d):
                print('\t%s' % p)
        return

    def _impl_lex(self, yy): #name, text):
        DEBUG = True
        def diagnostic_dump():
            h2 = sorted(h)
            for a, b, i, ph in h2:
                print('\t"%-10s" %4i-%4i %3i %s'
                      % ( text[a:-b][:10], a, -b, i, ph.dumps() ))
            return
        t, end = 0, len(text)
        h = [ ]
        for u, ph in enumerate(self._patterns):
            a, b = ph.search(text, t, end)
            if 0 <= a < b:
                # XXX silently drop empty matches
                h.append(( a, -b, u, ph ))
        heapq.heapify(h)
        if DEBUG:
            print('Initial setup:')
            diagnostic_dump()

        #yy = pylex.YY()
        while t < end:

            if not h:
                # no more matches
                a, b, i, ph = t, -end, 0, self._unmatched_handler
            else:
                a, b, i, ph = heapq.heappop(h)

            if (a > t):
                diagnostic_dump()

            yy._set_text(text, a, -b)
            t = -b

            terminal = ph.consume(yy)
            if terminal is not None:
                yield terminal

            a, b = ph.search(text, t, end)
            if t <= a < b:
                # XXX silently drop 0-len matches
                heapq.heappush(h, ( a, -b, i, ph ))

            while h:
                if h[0][0] >= t:
                    break
                a, b, i, ph = heapq.heappop(h)
                a, b = ph.search(text, t, end)
                if t <= a < b:
                    # XXX silently drop empty matches
                    heapq.heappush(h, ( a, -b, i, ph ))

        return


def lexer():
    return LexerFactory()

#---------------------------------------------------------------------#

def experiment():
    test = '\n'.join([ 'abc%i' % k
                       for k in range(1, 4+1) ])
    rx = re.compile(r'^abc\d+', re.MULTILINE)
    for i in range(len(test)):
        print(repr(test[i:]),
              rx.search(test, i).group())

if (__name__ == '__main__'):
    experiment()

#--#
