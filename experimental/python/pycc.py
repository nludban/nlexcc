#!/usr/local/bin/python3.5
## pycc.py

import pylexcc # import _find_callers_name

#grammar
# (tokens)
# rules

START_NAME = "S'"	# S'

class Rule: # production-rule

    def __init__(self, rfunc):
        self._rule = rfunc


class NonTerminalFactory(pylexcc.SymbolFactory):

    def __init__(self, grammar, name, union):
        pylexcc.SymbolFactory.__init__(self, name, union)
        self._grammar = grammar
        return

    def __enter__(self):
        return self

    def __exit__(self, type, value, tb):
        if value is not None:
            return False
        self._grammar = None
        return

    def rule(self, *symbols):
        if self._grammar is None:
            raise RuntimeError('Symbol definition context exited.')
        return self._grammar._impl_rule(self,
                                        self._union,
                                        symbols)

    def __repr__(self):
        return '<NonTerminalFactory(%s, %s)>' % ( self._name,
                                                  self._union )

    def __str__(self):
        return self._name

#    def __lt__(self, other):
#        return (self._name < other._name)

# XXX Do not like this level of obfuscation...
#   def __call__(self, *args, **kwargs):
#       return self._union(*args, **kwargs)


class Grammar: #ParserFactory?

    def __init__(self, lexer_factory):
        self._lexer_factory = lexer_factory
        self._epsilon = NonTerminalFactory(self,
                                           '/e/', #'\u03f5',
                                           None)
        with self._epsilon:
            pass # No rules.
        self._start_symbol = None
        return

    def _find_callers_name(self, n):
        name = pylexcc._find_callers_name(None, n + 1)
        # XXX conventional, non-duplicate name?
        # XXX (convention implies non-clashing with lexer)
        return name

    def __enter__(self):
        return self

    def __exit__(self, type, value, tb):
        if value is not None:
            return False
        # XXX Disable modifications to grammar...
        self._impl_preprocess()
        return

    def prec(self, *terminals):
        self._impl_prec(terminals)
        return

    def left(self, *terminals):
        self._impl_left(terminals)
        return

    def right(self, *terminals):
        self._impl_right(terminals)
        return

    def nonassoc(self, *terminals):
        self._impl_nonassoc(terminals)
        return

    def symbol(self, *, union=object):
        name = self._find_callers_name(1)
        return NonTerminalFactory(self, name, union)

    def start(self, *symbols, union=None):
        # Cons a production rule from the augmented start symbol to
        # each of the target symbols.  The rule is None indicating
        # the parser should pop and yield the top value.
        if not symbols:
            raise ValueError('Must define 1+ target rules.')
        #s = self._lexer_factory.start()
        with NonTerminalFactory(self, START_NAME, union) as f:
            for sym in symbols:
                # No action tells the parser to yield.
                f.rule(sym)(action=None)
        self._start_symbol = f
        return

    def start_symbol(self):
        if self._start_symbol is None:
            raise ValueError('Grammar forgot to define start symbols.')
        return self._start_symbol

    def epsilon_symbol(self):
        #https://en.wikipedia.org/wiki/Empty_string
        return self._epsilon

    def productions(self):
        # XXX Move class Production here?
        return self._impl_productions()

    def parser(self, lexer):
        return Parser(self, lexer)


class Stack:

    def __init__(self):
        self.clear()
        return

    def clear(self):
        self._visible = None
        self._symbols = [ ]
        self._states = [ ]
        return

    def shift(self, symbol, state):
        self._symbols.append(symbol)
        self._states.append(state)
        return

    def reduce(self, rule, n):
        self._visible = n
        try:
            nt = rule(self)
        finally:
            self._visible = None
        del self._symbols[-n:]
        del self._states[-n:]
        return nt

    def pop(self):
        sym = self._symbols[-1]
        del self._symbols[-1]
        del self._states[-1]
        return sym

    @property
    def symbol(self):
        return self._symbols[-1]

    @property
    def state(self):
        return self._states[-1]

    def __str__(self):
        return ' '.join([ str(x) for x in self._symbols ])

    # Use () instead of [] because not iterable or assignable
    def __call__(self, k):
        if not 1 <= k <= self._visible:
            raise IndexError(k)
        i = len(self._symbols) - self._visible + k - 1
        return self._symbols[i].value


class Parser:

    def __init__(self, grammar, lexer):
        self._grammar = grammar
        self._lexer_iter = lexer.lex()
        self._stack = Stack()
        return

    def parse(self):
        yield from self._grammar._impl_parse(self._lexer_iter,
                                             self._stack)

    # Intended as one-use parser...
    # Lexer already supports extending the input.
    # Error recovery / continuation?
    # - consume from lexer until some sequence of tokens is seen?
    # - restore stack to some state?

#shift_reduce_conflict
#prec[edence]
#assoc[iatiivty] - left/right/nonassoc

#yyparse()
#yyerror()
#yyin()

#--#
