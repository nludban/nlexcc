#!/usr/local/bin/python3.5

import pylex_re as pylex
import pycc_lr as pycc

import operator
import sys

test_txt = '''
print 42
print 1 + 2
print 6 * 7
print 1 + 2 * 3 + 4
print (1 + 2) * (3 + 4)
'''


with pylex.lexer() as calc_lexer:
    l = calc_lexer

    EOF		= l.eof()

    with l.token(union=int) as INTEGER:

        @INTEGER.pattern(r'\d+')
        def action(yy):
            '''Emit an integer'''
            print('-INTEGER: %r' % yy.text)
            return INTEGER(yy, int(yy.text, 10))

        def forty_two(yy):
            return INTEGER(yy, 42)
        INTEGER.literal('42')(forty_two)
        INTEGER.literal('0x2a')(forty_two)
        INTEGER.literal('052')(forty_two)
        INTEGER.literal('011010b')(forty_two)

    with l.token(union=float) as FLOAT:

        def action(yy):
            '''Emit a float'''
            print('-FLOAT: %r' % yy.text)
            return FLOAT(yy, float(yy.text))

        FLOAT.pattern(r'\d+\.\d+')(action)
        FLOAT.pattern(r'[-+]\d+\.\d+')(action)

    PRINT	= l.keyword('print')

    ADD		= l.punct('+')
    SUB		= l.punct('-')
    MUL		= l.punct('*')
    DIV		= l.punct('/')
    LPAREN	= l.punct('(')
    RPAREN	= l.punct(')')

    WS		= l.whitespace()

print(calc_lexer.dumps())
#sys.exit(0)

#---------------------------------------------------------------------#

class Expression:
    def eval(self): pass

class ConstantExpression(Expression):
    def __init__(self, c):
        self._c = c
        return
    def eval(self):
        return self._c

class BinaryExpression(Expression):
    def __init__(self, opfun, lhs, rhs):
        self._opfun = opfun
        self._lhs = lhs
        self._rhs = rhs
        return
    def eval(self):
        return self._opfun(self._lhs.eval(), self._rhs.eval())

class Statement:
    def exec(self): pass

class PrintStatement(Statement):
    def __init__(self, expr):
        self._expr = expr
    def exec(self):
        print(self._expr.eval())

#---------------------------------------------------------------------#


with pycc.grammar(calc_lexer) as calc_grammar:
    g = calc_grammar

    g.prec(MUL, DIV)
    g.prec(ADD, SUB)

    expr = g.symbol(union=Expression)

    with g.symbol(union=ConstantExpression) as const_expr:

        @const_expr.rule(FLOAT)
        def action(s):
            return ConstantExpression(s(1))

        @const_expr.rule(INTEGER)
        def action(s):
            return ConstantExpression(s(1))

    with g.symbol(union=BinaryExpression) as binary_expr:

        @binary_expr.rule(expr, ADD, expr)
        def action(s):
            return BinaryExpression(operator.add, s(1), s(3))

        @binary_expr.rule(expr, SUB, expr)
        def action(s):
            return BinaryExpression(operator.sub, s(1), s(3))

        @binary_expr.rule(expr, MUL, expr)
        def action(s):
            return BinaryExpression(operator.mul, s(1), s(3))

        @binary_expr.rule(expr, DIV, expr)
        def action(s):
            return BinaryExpression(operator.truediv, s(1), s(3))

    with g.symbol(union=Expression) as paren_expr:

        @paren_expr.rule(LPAREN, expr, RPAREN)
        def action(s):
            return s(2)

    with expr: #g.symbol(Expression) as expr:

        def action(s):
            return s(1)

        expr.rule(const_expr)(action)
        expr.rule(binary_expr)(action)
        expr.rule(paren_expr)(action)

    with g.symbol(union=Statement) as stmt:

        @stmt.rule(PRINT, expr)
        def action(s):
            return PrintStatement(s(2))

    with g.symbol(union=list) as stmt_list:

        @stmt_list.rule(stmt)
        def action(s):
            return [ s(1) ]

        @stmt_list.rule(stmt_list, stmt)
        def action(s):
            s(1).append(s(2))
            return s(1)

    with g.symbol() as program:

        @program.rule(stmt_list, EOF)
        def action(s):
            print('Read a program!')
            return s(1)

    ###print(g, g.start)
    g.start(program)

print(calc_grammar)

#---------------------------------------------------------------------#

lexer = calc_lexer.lexer()
lexer.push_input_text('test_txt', test_txt)
for tok in lexer.lex():
    print(tok)

lexer = calc_lexer.lexer()
parser = calc_grammar.parser(lexer)
lexer.push_input_text('test_txt', test_txt)
for stmt in parser.parse():
    stmt.exec()

#--#
