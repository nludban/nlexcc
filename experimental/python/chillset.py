#!/usr/local/bin/python3.5
## chillset.py
#
# Alternate set/frozenset implementation for pylexcc which
# frequently needs:
# - Iterate an algorithm until the result stops changing.
# - Update during iteration.
# - List of changes since the last update (debug).
# - List of members in canonical (sorted) order (validation).
# - List of members in insertion order (debug).
# - Efficient set operations (~100 members, sets of sets).

import bisect

#---------------------------------------------------------------------#

def _a_only(ai, bi):
    # For sorted sequences ai and bi,
    # return [ a for a in ai if a not in bi ]
    # XXX yield?
    ai = list(ai)
    bi = list(bi)
    j = [ ]
    while ai and bi:
        a = ai[0]
        b = bi[0]
        if a < b:
            j.append(a)
            del ai[0]
        elif a == b:
            del ai[0]
            del bi[0]
        else: # a > b
            del bi[0]
    j.extend(ai)
    return j

def _b_only(ai, bi):
    return _a_only(bi, ai)

def _a_and_b(ai, bi):
    # For sorted sequences ai and bi,
    # return [ a for a in ai if a in bi ]
    # XXX yield?
    ai = list(ai)
    bi = list(bi)
    j = [ ]
    while ai and bi:
        a = ai[0]
        b = bi[0]
        if a < b:
            del ai[0]
        elif a == b:
            j.append(a)
            del ai[0]
            del bi[0]
        else: # a > b
            del bi[0]
    return j

def _a_or_b(ai, bi):
    # For sorted sequences ai and bi,
    # return union(ai, bi)
    # XXX yield?
    ai = list(ai)
    bi = list(bi)
    j = [ ]
    while ai and bi:
        a = ai[0]
        b = bi[0]
        if a < b:
            j.append(a)
            del ai[0]
        elif a == b:
            j.append(a)
            del ai[0]
            del bi[0]
        else: # a > b
            j.append(b)
            del bi[0]
    j.extend(ai)
    j.extend(bi)
    return j

def _a_xor_b(ai, bi):
    # For sorted sequences ai and bi,
    # return merge(a_only(ai, bi), b_only(ai, bi))
    # XXX yield?
    ai = list(ai)
    bi = list(bi)
    j = [ ]
    while ai and bi:
        a = ai[0]
        b = bi[0]
        if a < b:
            j.append(a)
            del ai[0]
        elif a == b:
            del ai[0]
            del bi[0]
        else: # a > b
            j.append(b)
            del bi[0]
    j.extend(ai)
    j.extend(bi)
    return j

#---------------------------------------------------------------------#

class ChillSet:

    def __init__(self, *iters):
        self._frozen = False
        self._chilled = 0
        self._items = [ ]
        self._order = [ ]
        self.update(*iters)
        return

    def __len__(self):
        return len(self._items)

    def __contains__(self, item):
        n = bisect.bisect_left(self._items, item)
        return ((n < len(self._items)) and (self._items[n] == item))

    def find_inserted(self, item):
        assert self._frozen
        n = bisect.bisect_left(self._items, item)
        if ((n < len(self._items)) and (self._items[n] == item)):
            return self._order[n] - 1 # XXX 1..N -> 0..N-1
        return -1

    def find_sorted(self, item):
        assert self._frozen
        n = bisect.bisect_left(self._items, item)
        if ((n < len(self._items)) and (self._items[n] == item)):
            return n
        return -1

    def iter_inserted(self):
        # Make a shallow copy of the current list of members so
        # that the chillset can be modified during iteration.
        # Sorting by insertion order makes the LR algorithms
        # reproduce the textbook results.
        tmp = list(sorted(zip(self._order, self._items)))
        return ( t[1] for t in tmp )

    def iter_sorted(self):
        # Make a shallow copy of the current list of members so
        # that the chillset can be modified during iteration.
        tmp = list(self._items)
        return iter(tmp)

    def changes(self):
        tmp = [ (order, item)
                for item, order in zip(self._items, self._order)
                if order > self._chilled ]
        tmp.sort()
        return [ item for order, item in tmp ]

    def chill(self):
        if self._frozen:
            raise RuntimeError('ChillSet is frozen.')
        self._chilled = len(self._items)
        return self

    def isfrozen(self):
        return self._frozen

    def freeze(self):
        self._chilled = len(self._items)
        self._frozen = True
        return self

    def add(self, item):
        if isinstance(item, ChillSet) and not item.isfrozen():
            raise RuntimeError('Adding non-frozen ChillSet.')
        if self._frozen:
            raise RuntimeError('Adding to frozen ChillSet.')
        n = bisect.bisect_left(self._items, item)
        if ((n < len(self._items)) and (self._items[n] == item)):
            return
        self._items.insert(n, item)
        self._order.insert(n, len(self._items)) # 0..N-1
        return

    # No application requirement for remove(), would need to
    # replace order=len(items) with a generation counter.
    # Then also add a touch(item) method?

    def __eq__(self, other):
        if not isinstance(other, ChillSet):
            raise ValueError('Comparison ChillSet to other.')
        if len(self._items) != len(other._items):
            return False
        return all( a == b for a, b in zip(self._items, other._items) )

    def __lt__(self, other):
        if not isinstance(other, ChillSet):
            raise ValueError('Comparison ChillSet to other.')
        for a, b in zip(self._items, other._items):
            if a < b:
                return True
            if b < a:
                return False
        return len(self._items) < len(other._items)

    def update(self, *others):
        for other in others:
            if isinstance(other, ChillSet):
                for item in other.iter_sorted():
                    self.add(item)
            else:
                for item in other:
                    self.add(item)
        return

    def __sub__(self, other):
        return ChillSet(_a_only(self._items, other._items))

    def __str__(self):
        # Canonical (sorted) order.
        return '{ %s }' % ', '.join([
            '%s%s' % ( item, ('~' if order > self._chilled else '') )
            for item, order in zip(self._items, self._order) ])

    def __repr__(self):
        # First insertion order.
        tmp = [ (order, item)
                for item, order in zip(self._items, self._order) ]
        tmp.sort()
        return '{ %s }' % ', '.join([
            '%s%s' % ( item, ('~' if order > self._chilled else '') )
            for order, item in tmp ])

#---------------------------------------------------------------------#

def test():
    a = ChillSet()
    print(repr(a))
    print(a)

    a = ChillSet([ 1, 2, 3 ]).chill()
    print('1-2-3 ?', repr(a))

    a = ChillSet([ 1, 2, 3 ]).chill()
    a.add(10)
    print('1-2-3-10 ?', repr(a))

    a = ChillSet([ 1, 2, 3 ]).chill()
    a.add(-10)
    print('-10-1-2-3 ?', repr(a))

    a = ChillSet([ 1, 2, 3 ]).chill()
    a.add(0)
    print('0-1-2-3 ?', repr(a))

    a = ChillSet([ 1, 2, 3 ]).chill()
    a.add(4)
    print('1-2-3-4 ?', repr(a))

    a = ChillSet([ 1, 2, 3 ]).chill()
    a.add(1)
    print('1-2-3 ?', repr(a))

    a = ChillSet([ 1, 2, 3 ]).chill()
    a.add(2)
    print('1-2-3 ?', repr(a))

    a = ChillSet([ 1, 2, 3 ]).chill()
    a.add(3)
    print('1-2-3 ?', repr(a))

    a = ChillSet([ 1, 2, 3 ]).chill()
    b = ChillSet([ 2, 4 ])
    print('1-3 ?', a - b)

    return

if (__name__ == '__main__'):
    test()

#--#
