#!/usr/local/bin/python3.5
## core_lr.py - stuff shared between LR algorithms.

import pylexcc
import pylex
import pycc

from chillset import ChillSet

def dumps(x):
    if isinstance(x, ChillSet):
        return '{ %s }' % ('\n\t'.join([ str(y) for y in sorted(x) ]))
    return str(x)

class Production:

    def __init__(self, nonterminal, symbols, action):
        # Production rule:
        #	name -> *symbols

        assert isinstance(nonterminal, pycc.NonTerminalFactory), \
            repr(nonterminal)
        self.nonterminal = nonterminal

        assert all([ isinstance(s, pylexcc.SymbolFactory)
                     for s in symbols ])
        self.symbols = symbols

        self.action = action
        return

#    def yields(self, name):
#        return self.name == name

#    def startswith(self, name):
#        return self.symbols[0].name == name

    def __len__(self):
        return 1 + len(self.symbols)

    def __hash__(self):
        return hash(self.nonterminal) ^ hash(self.symbols)

    def __eq__(self, other):
        return ((self.nonterminal == other.nonterminal)
                and (self.symbols == other.symbols))

    def __lt__(self, other):
        return ((self.nonterminal < other.nonterminal)
                or ((self.nonterminal == other.nonterminal)
                    and (self.symbols < other.symbols)))

    def __getitem__(self, n):
        assert 0 <= n <= len(self.symbols)
        return self.nonterminal if n == 0 else self.symbols[n - 1]

    def __str__(self):
        return '%s -> %s' % (
            self.nonterminal,
            ' '.join([ str(s) for s in self.symbols ]),
        )

    def __repr__(self):
        return '<Production %s %r>' % (
            self,
            pylexcc._action_repr(self.action),
        )


class Item:
    # Given the standard item notation:
    #
    #	[ A -> alpha . beta, a ]
    #
    # nonterminal = A
    # ldot = alpha (a tuple)
    # rdot = beta (a tuple)
    # lookahead = a

    def __init__(self, nonterminal, ldot, rdot, lookahead=None):
        assert isinstance(nonterminal, pycc.NonTerminalFactory)
        assert isinstance(ldot, tuple)
        assert all([ isinstance(X, pylexcc.SymbolFactory)
                     for X in ldot ])
        assert isinstance(rdot, tuple)
        assert all([ isinstance(X, pylexcc.SymbolFactory)
                     for X in rdot ])
        assert lookahead is None or isinstance(lookahead,
                                               pylex.TerminalFactory)

        self.nonterminal = nonterminal
        self.ldot = tuple(ldot)
        self.rdot = tuple(rdot)
        self.lookahead = lookahead
        return

    def __repr__(self):
        return '[ %s -> %s . %s, %s ]' % (
            self.nonterminal,
            ' '.join([ str(x) for x in self.ldot ]),
            ' '.join([ str(x) for x in self.rdot ]),
            self.lookahead)

    def __hash__(self):
        return hash(( self.nonterminal, self.ldot, self.rdot ))

    def __eq__(self, other):
        return ((self.nonterminal == other.nonterminal)
                and (self.ldot == other.ldot)
                and (self.rdot == other.rdot)
                and (self.lookahead == other.lookahead))

    def __lt__(self, other):
        return (
            ( self.nonterminal, self.ldot,
              self.rdot, self.lookahead )
            < ( other.nonterminal, other.ldot,
                other.rdot, other.lookahead ))

    def __len__(self):
        # Bogus number, supports ChillSet ordering
        return len(self.ldot) * 100 + len(self.rdot)

    def __getitem__(self, n):
        if n == 0:
            return self.name
        if n > 0:
            return self.ldot[n - 1]
        if n < 0:
            # rdot = (a, b, c)
            # -->
            # i[-1] = a, i[-2] = b, i[-3] = c
            return self.rdot[-1 - n]


#--#
