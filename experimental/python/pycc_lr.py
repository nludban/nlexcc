#!/usr/local/bin/python3.5
## pycc_lr.py

import warnings

import pylexcc
import pylex
import pycc

from chillset import ChillSet

from core_lr import Production
from core_lr_1 import mktables_LR_1

#---------------------------------------------------------------------#

class Grammar(pycc.Grammar): #ParserFactory

    def __init__(self, lexer_factory, mktables):
        pycc.Grammar.__init__(self, lexer_factory)
        self._mktables = mktables
        self._productions = [ ]
        #self._constraints = [ ]
        self._states = None
        self._parsing_table = None
        return

    def _impl_prec(self, terminals):
        warnings.warn('LR parser ignoring precedence (@prec)',
                      UserWarning)

    def _impl_left(self, terminals):
        warnings.warn('LR parser ignoring associativity (@left)',
                      UserWarning)

    def _impl_right(self, terminals):
        warnings.warn('LR parser ignoring associativity (@right)',
                      UserWarning)

    def _impl_nonassoc(self, terminals):
        warnings.warn('LR parser ignoring associativity (@nonassoc)',
                      UserWarning)

    def _impl_rule(self, ntf, union, symbols):
        # In the example:
        #
        # with g.symbol(union=(BinaryExpression) as binary_expr:
        #   @binary_expr.rule(expr, ADD, expr)
        #   def action(s):
        #     return BinaryExpression(operators.add, s(1), s(3))
        #
        # ntf = binary_expr (is-a NonTerminalFactory)
        # union = BinaryExpr
        # symbols = (expr, ADD, expr) (all is-a SymbolFactory)
        # action = def action(s)...
        assert isinstance(symbols, tuple)
        if not symbols:
            symbols = (self.epsilon(),)
        def deco(action):
            if action is None:
                # Start rule.  Should this even be reachable?
                # XXX Is reached...
                pr = Production(ntf, symbols, None)
                self._productions.append(pr)
                return None
            def wrap_action(stack):
                # pr.action() comes here...
                value = action(stack)
                if not isinstance(value, union):
                    raise RuntimeError('Reduction action bad result...')
                nt = pylexcc.NonTerminal(ntf.name, union, value)
                return nt
            pr = Production(ntf, symbols, wrap_action)
            self._productions.append(pr)
            return wrap_action # Bogus, not used.
        return deco

    #--------------------------------------------------#

    def _impl_productions(self):
        G = ChillSet(self._productions).freeze()
        assert len(G) == len(self._productions)
        return G

    def _impl_preprocess(self):
        ( self._rules,
          self._states,	# Info only.
          self._state_0,
          self._actions,
          self._gotos ) = self._mktables(
              self._lexer_factory,
              self)
        return

    def _impl_parse(self, lexer, stack):

        stack.clear()
        stack.shift(None, self._state_0)

        shifted = True
        while True:
            if shifted:
                try:
                    tok = next(lexer)
                except StopIteration:
                    break
                print('LR token=%s' % tok)
                shifted = False

            cs = stack.state
            print('LR in %i, stack= %s . %s' % (
                cs,
                str(stack),
                tok ))

            shift_i, reduce_j, other = self._get_action(cs, tok)

            if shift_i is not None:
                print('LR shift %r' % tok)
                print(' + goto %i' % shift_i)
                stack.shift(tok, shift_i)
                shifted = True
                continue

            if reduce_j is not None:
                pr = self._rules[reduce_j]
                nt = stack.reduce(pr.action, len(pr.symbols))
                print('LR reduced %s; got %r' % ( pr, nt ))
                goto_k = self._get_goto(stack.state, pr.nonterminal)
                print(' + goto %i' % goto_k)
                stack.shift(nt, goto_k)
                continue

            if other == 'accept':
                yield stack.pop()
                stack.clear()
                stack.shift(None, self._state_0)
                if tok.iseof():
                    break
                continue

            if other == 'error':
                raise XXX

            raise XXX # Bad self._get_action()

        if stack.state != self._state_0:
            raise XXX # Missing EOF?
        return

    def _get_action(self, state, token):
        for a, what in self._actions[state]:
            # a is-a NonTerminalFactory
            # token is-a NonTerminal
            if a.name == token.name:
                break
        else:
            #raise KeyError('No action for state+token.')
            return None, None, 'error'
        shift_i = reduce_j = other = None
        if what.startswith('shift '):
            shift_i = int(what.split()[-1])
        if what.startswith('reduce '):
            reduce_j = int(what.split()[-1])
        if what.startswith('accept'):
            other = 'accept'
        return shift_i, reduce_j, other

    def _get_goto(self, state, nonterminal):
        g = self._gotos[state]
        for nt, k in g:
            if nt == nonterminal:
                return k
        raise KeyError('No goto for state+nonterminal.')

#---------------------------------------------------------------------#

def grammar(lexer_factory, mode='LR(1)'):
    return Grammar(lexer_factory, mktables_LR_1) #lexer_factory)

#--#
