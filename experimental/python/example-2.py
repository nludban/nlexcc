#!/usr/local/bin/python3.5

import pylex_re as pylex
import pycc_lr as pycc

import operator
import sys

with pylex.lexer() as test_lexer:
    l = test_lexer

    c = l.keyword(literal='si')
    d = l.keyword(literal='dee')

with pycc.grammar(test_lexer) as test_grammar:
    g = test_grammar

    with g.symbol() as C:

        @C.rule(c, C)
        def action(s):
            return 42

        @C.rule(d)
        def action(s):
            return 23

    with g.symbol() as S:

        @S.rule(C, C)
        def action(s):
            return -1

    g.start(S)

#---------------------------------------------------------------------#
'''
Patterns:
        c := ``si''  # Basic keyword emitter
        d := ``dee''  # Basic keyword emitter

Grammar production rules:
        C -> c C
        C -> d
        S -> C C
        S' -> S

Firsts:
        $ : $
        /e/ : /e/
        C : c d
        S : c d
        S' : c d
        c : c
        d : d

Follows:
        C : $ c d
        S : $
        S' : 

Sets of items:
-0-     [ S' ->  . S, $ ]
        [ S ->  . C C, $ ]
        [ C ->  . c C, c ]
        [ C ->  . c C, d ]
        [ C ->  . d, c ]
        [ C ->  . d, d ]
-1-     [ S -> C . C, $ ]
        [ C ->  . c C, $ ]
        [ C ->  . d, $ ]
-2-     [ S' -> S . , $ ]
-3-     [ C -> c . C, c ]
        [ C -> c . C, d ]
        [ C ->  . c C, c ]
        [ C ->  . d, c ]
        [ C ->  . c C, d ]
        [ C ->  . d, d ]
-4-     [ C -> d . , c ]
        [ C -> d . , d ]
-5-     [ S -> C C . , $ ]
-6-     [ C -> c . C, $ ]
        [ C ->  . c C, $ ]
        [ C ->  . d, $ ]
-7-     [ C -> d . , $ ]
-8-     [ C -> c C . , c ]
        [ C -> c C . , d ]
-9-     [ C -> c C . , $ ]
'''

#--#
