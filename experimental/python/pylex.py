#!/usr/local/bin/python3.5
## pylex.py

import inspect
import re
import sys

import pylexcc


class YY:
    '''Holds the lexer state, traditional lex globals (eg, yytext)
    become attributes or functions here (less the "yy" prefix).

    Designed to be subclassed to alter behaviour.
    '''

    def __init__(self):
        self.__at_bol = True
        self.__text = ''
        self.__more = ''
        self.__lexer = None
        self.__debug_level = 0
        self.__states = [ ]
        self.__buffer = None
        return

    #yy_scan_string()
    def scan_string(self, source, text):
        '''Create a StringBuffer and set it as the current input.

        Follows the pattern of yy_scan_string().
        '''
        buffer = StringBuffer(source, text)
        self.switch_to_buffer(buffer)
        return buffer

    #yy_create_buffer()
    def create_buffer(self, source, fobj):
        '''Create a FileBuffer and return it.

        Follows the pattern of yy_create_buffer().
        '''
        buffer = FileBuffer(source, fobj)
        return buffer

    # start: yyin = fobj
    # during: restart(fobj)
    def switch_to_buffer(self, buffer):
        '''Change to a new input buffer.
        '''
        assert isintsance(buffer, Buffer)
        self.__buffer = buffer
        return

    #YY_CURRENT_BUFFER
    @property
    def current_buffer(self):
        '''Gets the currently active buffer.'''
        return self.__buffer

    @property
    def text(self):
        '''Gets the currently matched text, including any text
        saved with yy.more().'''
        return self.__text

    @property
    def leng(self):
        '''yyleng, same as len(yy.text).'''
        return len(self.__text)

    #YY_AT_BOL() / yy_set_bol(bool)
    @property
    def at_bol(self):
        '''True when at "beginning of line", defined as just after
        a newline or by the application using yy.set_bol().'''
        return self.__at_bol

    def set_bol(self, yes_no):
        '''Set the "beginning of line" flag for the next match.'''
        self.__at_bol = bool(yes_no)
        self._lexer._impl_set_bol(self.__at_bol)
        return

#   #yylval
#   def lval(self, x):
#       warnings.warn('yy.lval is a no-op.')
#       return x

    #BEGIN(<state|0>)
    def begin(self, state):
        '''Set a new state, clearing any previous states from
        the stack.'''
        if state is 0: # ???
            state = self.__lexer.initial()
        self.__state = [ state ]
        return

    # YYSTATE (flex) (was YY_START in lex)
    @property
    def state(self):
        '''Get the current state.'''
        return self.__states[-1]

    #yy_push_state()
    def push_state(self, state):
        '''Push a state onto the stack, making it active.'''
        self._states.append(state)
        return

    #yy_pop_state()
    def pop_state(self):
        '''Remove the current state from the stack and return it.
        The previous state (if any) is restored, otherwise it
        returns to INITIAL.
        '''
        s = self.__states[-1]
        del self.__states[-1]
        if not self._states:
            self.begin(0)
        return s

    #yy_top_state()
    def top_state(self):
        '''Get the current state.
        '''
        return self.__states[-1]

    #return yy.more() -> next yytext = current match + next match
    # Use case: C-style string escape state
    # optional alternate text (cumulative), default no change
    def more(self, text=None):
        '''Save text (by default, the currently matched) and prepend
        it to the next match.

        This implementation allows an action to call yy.more()
        followed by returning a token.
        '''
        self.__more = self.__text if text is None else text
        return

    #less(n) -> return all but first n chars to input
    def less(self, n):
        '''Return all but the first N matched characters to the
        input buffer to be sacnned again.

        Note that yy.less(0) may cause an infinite loop unless
        the state is also changed.

        This implementation allows an action to call yy.less()
        followed by returning a token.
        '''
        self.__lexer._impl_less(n)
        return

    #return terminate() -> instead of terminal
    # Use case: stop all scanning instruction.
    def terminate(self):
        '''
        '''
        self.__lexer._impl_terminate()
        return None

    #--------------------------------------------------#

    #unput(c) -> put c at front of input
    # Does not fit with implementation.

    #--------------------------------------------------#

    def debug(self, level=None):
        prev_level = self.__debug_level
        if level is not None:
            self.__debug_level = level
        return prev_level

    # Extension: access the current regex match object,
    # eg to get its groups.
    @property
    def match(self):
        if self.__match is None:
            raise RunTimeError('Literal has no match object.')
        return self.__match

    #return yy.macro(seq) -> yield from iterable.

    #source? (filename)

    #--------------------------------------------------#

    def _set_lexer(self, lexer):
        assert isinstance(lexer, Lexer)
        self.__lexer = lexer
        return

    def _set_text(self, text, a, b, at_bol, m=None):
        if self.__more:
            self.__text = self.__more + self.__text[a:b]
            self.__more = ''
        else:
            self.__text = text[a:b]
        self.__at_bol = at_bol
        self.__match = m
        return

    #--------------------------------------------------#

    #yyout, ECHO
    def echo(self, *text):
        if not text:
            text = ( self.__text, )
        for t in text:
            print('{:s}'.format(t))
        return

    #yywrap() 0=continue next file
    def wrap(self):
        # self.scan_string(...)
        # -or-
        # self.switch_to_buffer(...)
        # -or-
        return True # End of input.

    # Default action for invalid input.  Override to do
    # something different.
    def unmatched(self):
        text = self.text
        raise ValueError('Unmatched %i input chars looking at %r'
                         % ( len(text), text ))


    #<*> <<EOF>>
    #yy_init_buffer ?
    #yy_load_buffer_state ?


class Buffer:

    #YY_BUF_SIZE = ?

    def __init__(self, source):
        self._source = source # eg: filename
        return

    def text_a_b(self):
        raise NotImplementedError

    def delete(self):
        pass

    def flush(self):
        # discard contents, implies input()
        raise NotImplementedError()

    def input(self, consumed, size_hint):
        raise NotImplementedError()


class StringBuffer(Buffer):

    def __init__(self, source, text):
        Buffer.__init__(self, source)
        self._text = text
        self._a = 0
        self._b = len(text)
        return

    def text_a_b(self):
        return ( self._text, self._a, self._b )

    def delete(self):
        del self._text, self._a, self._b
        return

    def flush(self):
        self._a = self._b
        return

    def input(self, consumed, size_hint):
        self._a += consumed
        return


class FileBuffer(Buffer):

    def __init__(self, source, fobj):
        Buffer.__init__(self, source)
        self._fobj = fobj
        self._eof = False
        self.flush()
        return

    def text_a_b(self):
        return ( self._text, self._a, self._b )

    def delete(self):
        self._fobj.close()
        self.flush()
        return

    def flush(self):
        self._text = ''
        self._a = 0
        self._b = 0
        return

    def input(self, consumed, size_hint):
        self._a += consumed
        if self._a >= size_hint:
            self._text = self._text[size_hint:]
            self._a -= size_hint
            self._b -= size_hint
        while self._b - self._a < size_hint and not self._eof:
            t = self._fobj.read(8192)
            if not t:
                self._eof = True
                break
            self._text += t
            self._b += len(t)
        return


class State:

    def __init__(self, name):
        self.name = name
        return

    #__enter__ -> push default state (union?)

    def __str__(self):
        return '<%s>' % self.name


class TerminalFactory(pylexcc.SymbolFactory):
    '''Produces pylexcc.Terminal instances.

    Not to be instantiated directly by the user application, use
    Lexer.token or any of the Lexer convenience methods.
    '''

    def __init__(self, lexer, name, union):
        pylexcc.SymbolFactory.__init__(self, name, union)
        self._lexer = lexer
        return

    def __enter__(self):
        'Begin defining actions.'
        return self

    def __exit__(self, type, value, tb):
        'End defining actions.'
        if value is not None:
            return False
        self._lexer = None
        return

    def pattern(self, pattern, flags=re.MULTILINE):
        '''Get a decorator for an action using a pattern
        (regular expression string).'''
        return self._lexer._impl_pattern(pattern, flags, self)

    def literal(self, literal):
        '''Get a decorator for an action using a literal string.'''
        return self._lexer._impl_literal(literal, self)

    def __call__(self, yy, *value):
        '''Create a Terminal with the correct symbol_name and
        union value.

        Requires one additional value for tokens defining a
        union field type.
        '''
        ###print('u=%r v=%r' % ( self._union, value ))
        if self._union is not None:
            value, = value
            if not isinstance(value, self._union):
                raise TypeError('Lexer action bad result...')
        else:
            if value:
                raise TypeError('Lexer action bad result...')
            value = None
        return pylexcc.Terminal(self._name, self._union, value)

#    def __lt__(self, other):
#        return (self._name < other._name)

    def __str__(self):
        return self._name

    def __repr__(self):
        return '<TerminalFactory(%s, %s)>' % ( self._name,
                                               self._union )


class EofFactory(TerminalFactory):

    def __init__(self, lexer):
        TerminalFactory.__init__(self, lexer, '$', None)
        #self._lexer = lexer
        #self._name = self.EOF_NAME
        #self._union = union
        return

    def __call__(self): #, yy):
        return pylexcc.Terminal(self.name, None, None)

    def __str__(self):
        return '$'

    def __repr__(self):
        return '<EofFactory($)>'


class LexerFactory:
    '''Producer of Lexer instances.
    Context Manager for defining a Lexer?
    '''

    def __init__(self):
        self._eof = EofFactory(self)
        #self._unmatched_action = None
        self._states = [ State('INITIAL') ]
        return

    def __enter__(self):
        'Begin defining rules.'
        return self

    def __exit__(self, type, value, tb):
        'End defining rules.'
        if value is not None:
            return False
        self._impl_preprocess() #self._unmatched_action)
        pass # TODO: Enforce the end.

    #def literal(self, literal): def wrap(f):
    #def pattern(self, pattern): def wrap(f):

    def _find_callers_name(self, name, n):
        name = pylexcc._find_callers_name(name, n + 1)
        # XXX conventional, non-duplicate name?
        return name

    # TODO:
    #
    # pgs 42-48:
    #	<STATE>pattern { }
    #	{ BEGIN STATE|0; }
    # ->
    #	S_XYZZY = l.state() # name="<S_XYZZY>"
    #	l.token(..., state=S_XYZZY)...
    #	yy.begin(S_XYZZY)
    #	Could do "with S_XYZZY" but would force scattering of
    #	definitions of similar patterns all over the source...
    #
    # default (no match) action (all input to first match, EOF flag)
    #
    # re.compile flags (eg, case-insensitive)
    #
    # push/open_input_text()
    # pop/end/close_input_text()
    # switch_input_text() = pop+push
    # new input matches ^ (default)
    # push/switch with alternate initial state (default 0?)
    #
    # all: literal|pattern to explicit keyword (pick one)
    #
    # all: comment="Pattern meaning" -> "action docstring"
    #
    # all: optional echo=True; optional discard=True
    #	optional action=func instead of (...)(func)
    #
    # all: name='x' for arbitrary (punctuation); add quotes?
    #
    # yy.lineno, yy.charno, yy.column (property, expands tabs)
    #   No.
    # yy.match (re.match object for groups)
    #
    # space counting indent/dedenter?
    # -> general macro regurgitation facility?

    def initial(self):
        return self._states[0]

    def state(self, *, name=None):
        name = self._find_callers_name(name, 1)
        s = State(name, len(self._states))
        self._states.push(s)
        return s

    def token(self, *, name=None, union=None):
        '''Get a new TerminalFactory to be configured by the
        application'''
        name = self._find_callers_name(name, 1)
        return TerminalFactory(self, name, union)

    def discard(self, pattern, *, name=None, flags=0):
        '''Convenience method: get a new TerminalFactory configured
        to match a pattern and discard.
        '''
        name = self._find_callers_name(name, 1)
        with TerminalFactory(self, name, None) as f:
            @f.pattern(pattern, flags=flags)
            def action(yy):
                '''Basic discarder'''
                return None
        return f

    def keyword(self, literal, *, name=None):
        '''Convenience method: get a new TerminalFactory configured
        to match a literal keyword string.
        '''
        name = self._find_callers_name(name, 1)
        with TerminalFactory(self, name, None) as f:
            @f.literal(literal)
            def action(yy):
                '''Basic keyword emitter'''
                return f(yy)
        return f

    def punct(self, literal, *, name=None):
        '''Convenience method: get a new TerminalFactory configured
        to match a literal punct[uation] string.
        '''
        name = self._find_callers_name(name, 1)
        with TerminalFactory(self, name, None) as f:
            @f.literal(literal)
            def action(yy):
                '''Basic punctuation emitter'''
                return f(yy)
        return f

    def eof(self):
        return self._eof

#    def unmatched(self):
#        def deco(action):
#            self._unmatched_action = action
#            return action
#        return deco

    def terminals(self):
        return self._impl_terminals()

    def lexer(self):
        '''Create a Lexer instance using the rules defined
        on this LexerFactory.'''
        return Lexer(self)


class Lexer:

    def __init__(self, factory):
        self._factory = factory
        self._stack = [ ]
        return

    def push_input_text(self, name, text):
        self._stack.append(self._factory._impl_lex(name, text))
        return

    def lex(self):
        while self._stack:
            try:
                yield next(self._stack[-1])
            except StopIteration:
                del self._stack[-1]
        yield self._factory.eof()() #EofFactory._eof_token()
        return

#yyleng (int)
#yywrap (on EOF)

#--#
