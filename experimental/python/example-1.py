#!/usr/local/bin/python3.5

import pylex_re as pylex
import pycc_lr as pycc

import operator
import sys

with pylex.lexer() as math_lexer:
    l = math_lexer

    with l.token(union=float) as NUMBER:

        @NUMBER.pattern(r'\d+')
        def action(yy):
            '''Emit a number'''
            return NUMBER(yy, float(yy.text))

    LPAREN	= l.punct('(', name='(')
    RPAREN	= l.punct(')', name=')')
    ADD		= l.punct('+', name='+')
    MUL		= l.punct('*', name='*')
    SEMI	= l.punct(';', name=';')

    with l.token(name='#') as WS:

        @WS.pattern(r'[\s\n]+')
        def action(yy):
            return None

    #@l.unmatched()
    def action(yy):
        raise xyzzy

with pycc.grammar(math_lexer) as math_grammar:
    g = math_grammar

    expr = g.symbol()

    with g.symbol() as factor:

        @factor.rule(NUMBER)
        def action(s):
            return s(1)

        @factor.rule(LPAREN, expr, RPAREN)
        def action(s):
            return s(2)

    with g.symbol() as term:

        @term.rule(factor, MUL, factor)
        def action(s):
            return s(1) * s(3)

        @term.rule(factor)
        def action(s):
            return s(1)

    with expr:

        @expr.rule(term, ADD, term)
        def action(s):
            return s(1) + s(3)

        @expr.rule(term)
        def action(s):
            return s(1)

    with g.symbol() as stmt:

        @stmt.rule(expr, SEMI)
        def action(s):
            return s(1)

    with g.symbol() as S:
        @S.rule(stmt)
        def action(s):
            return s(1)

    g.start(S)

#---------------------------------------------------------------------#

test_txt = '2 + 4 * 10;'
#test_txt = '1 + 1 + 4 * 10;'

lexer = math_lexer.lexer()
parser = math_grammar.parser(lexer)
lexer.push_input_text('test_txt', test_txt)
for x in parser.parse():
    print('result= %r' % x)

#--#
