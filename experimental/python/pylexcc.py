#!/usr/local/bin/pythen3.5
## pylexcc.py

import inspect
import re

def _action_repr(f):
    return f.__doc__ if f.__doc__ else repr(f)

_unique_ids = 100
def _find_callers_name(name, frameno):
    if name is not None:
        return (name
                #if re.match(r'[0-9a-fA-F_]+$', name)
                if '.' not in name
                else "'%s'" % name)
    f = inspect.stack()[frameno+1]
    ctx = ' '.join(f.code_context)

    # foo = ?
    m = re.match(r'\s*([a-zA-Z_][a-zA-Z_0-9]*)\s*=', ctx)
    if m:
        return m.group(1)

    # with ? as foo:
    m = re.match(r'\s*with\s.*as\s+([a-zA-Z_][a-zA-Z_0-9]*):', ctx)
    if m:
        return m.group(1)

    global unique_ids
    unique_ids += 1
    name = '_UID_%i_' % unique_ids
    return name

#def _union_name_type(u):
#    if isinstance(u, str):
#        return ( u, object )
#    name, kind = u
#    if isinstance(name, str) and isinstance(kind, type):
#        return ( name, kind )
#    raise ValueError('Invalid name/type %r' % u)


class SymbolFactory:

    def __init__(self, name, union):
        assert isinstance(name, str)
        assert union is None or isinstance(union, type)
        self._name = name
        self._union = union
        return

    @property
    def name(self):
        '''The value of the name property of symbols produced
        by this factory.

        This property exists for the parser implementation to inspect
        (the factory is used when defining grammar rules).
        '''
        return self._name

    @property
    def union(self):
        '''The union field name of symbols produced by this factory.

        This property exists for the parser implementation to inspect
        (the factory is used when defining grammar rules).
        '''
        return self._union

    # Support hashable protocol for use in sets (of symbols):
    def __hash__(self):
        return hash(self.name)
    def __eq__(self, other):
        return self.name == other.name

    def __lt__(self, other):
        return self.name < other.name

#subclass TerminalFactory defined in pylex,
#	and further subclass EofFactory.
#subclass NonTerminalFactory defined in pycc,
#	and further subclass EpsilonFactory.

class Symbol:

    pass


class Terminal(Symbol):
    # Produced by the Lexer.
    # XXX Could be a namedtuple, but leave open for parser annotation...

    def __init__(self, name, union, value):
        self.name = name	# terminal NAME
        self.union = union	# union field type
        self.value = value	# union field value
        return

    def getattr(self, key):
        if key in ( '_', self._union ):
            return self._value
        raise AttributeError('Incorrect union name %r.' % key)

    def iseof(self):
        return self.name == '$'

    def __repr__(self):
        if self.value is None:
            #return '<%s>' % self.name
            return self.name
        return '<%s %r>' % ( self.name, self.value )


class NonTerminal(Symbol):
    # Produced by the Parser.

    def __init__(self, name, union, value):
        self.name = name	# symbol name
        self.union = union	# union field type
        self.value = value	# union field value
        return

    def getattr(self, key):
        if key in ( '_', self._union ):
            return self._value
        raise AttributeError('Incorrect union name %r.' % key)

    def __repr__(self):
        return '%s(%r)' % ( self.name, self.value )


# XXX exceptions: Error, LexerError, ParserError, ...
# XXX definition time vs execution time?

#--#

