#!/usr/local/bin/python

import operator
import re

source = '''

REM This is a comment.

REM Simple arithmetic expressions.
100 print 42
110 print 1 + 1
120 print 2 * 2
130 print 1 + 1 + 2 * 5 * 4

REM Expressions in parentheses.
200 print (1 + 1)
210 print (1 + 1) * (2 * 2)
220 print (1 + (1 * 2 * 2))
220 print (((((1 + (1 * 2 * 2))))))

REM Jump to a different line next.
300 goto 320
310 print 310
320 print 320

REM Variable assignment and usage.
400 x := 21
410 print x
420 print (x + x) * 10
430 x := 99
440 print x

REM Conditional branching.
500 when 5 < 10 goto 520
510 print 510
520 print 520
530 when 5 = 10 goto 550
540 print 540
550 print 550

REM Calculate 42 / 9, get quotient and remainder.
600 n := 42
610 d := 9
620 q := 0
630 when n < d goto 670
640 n := n - d
650 q := q + 1
660 goto 630
670 r := n
680 print q
690 print r

REM Branch through a variable.
700 z := 100
710 goto z
'''

# Using regular expressions:
# - Remove comments (REMarks) from the text.
# - Remove blank lines from the text.
# - Create a program data structure by splitting the remaining
#   text into the program / a list of tuples: (line_number, text)
program = [ ]

# Verify the line numbers are unique and increasing.

# Implement functions to:
# - Get the first line number.
# - Get the next line number after a given line number.
# - Get the text for a given line number.

#---------------------------------------------------------------------#

class VM:
    # A virtual machine defining a stack of working values (registers?)
    # and named variables the program can use for storage.

    def __init__(self):
        self._vars = { }
        self._stack = [ ]
        return

    def shift(self, value):
        # Push a value onto the stack.
        self._stack.append(value)
        return

    def reduce(self, n):
        # Remove N values from the stack.
        tmp = self._stack[-n:]
        del self._stack[-n:]
        return tmp

    def store(self, key):
        # Remove the top value and store in a variable.
        assert re.match(r'[a-z]$', key)
        value, = self.reduce(1)
        self._vars[key] = value
        return

    def load(self, key):
        # Shift (push) a variable onto the stack.
        assert re.match(r'[a-z]$', key)
        self.shift(self._vars[key])
        return

#---------------------------------------------------------------------#

def run_program(program):
    vm = VM()
    p = get_first_line_number(program)
    while True:
        text = get_line_number(program, p)
        q = exec_statement(vm, text)
        assert len(vm._stack) == 0
        if q is None:
            q = get_next_line_number(program, q)
        if q is None:
            break
        p = q
    return

#--------------------------------------------------#

def exec_statement(vm, text):
    # Use a regular expression to split the text
    # matching one of four statement patterns:
    #
    #	"print" expression
    #	"goto" number
    #	id ":=" expression
    #	"when" condition "goto" number
    #
    # Return None to advance to the next line in the program,
    # or a number to branch.
    pass

def exec_print(vm):
    value = vm.pop()
    print(value)
    return None

def exec_goto(vm, number):
    return number

def exec_assignment(vm, key):
    vm.store(key)
    return None

def exec_conditional(vm, condition, number):
    # Split condition into three parts:
    #    expression1 relop expression2
    eval_expression(vm, expression1)
    eval_expression(vm, expression2)
    x, y = vm.reduce(2)
    #if x relop y: return number
    return None

#--------------------------------------------------#

def eval_expression(vm, text):

    # Phase 1: evaluate left-to-right
    #
    # Example:
    #
    #    a + b * c + d
    #
    # vm.load('a')
    # vm.load('b')
    # x, y = vm.reduce(2)
    # vm.shift(x + y)   # a + b
    # vm.load('c')
    # x, y = vm.reduce(2)
    # vm.shift(x * y)   # (a + b) * c
    # vm.load('d')
    # x, y = vm.reduce(2)
    # vm.shift(x + y)   # ((a + b) * c) + d
    while text:
        pass


    # Phase 2: Check for text starting with "(" before
    # looping; allow optional ")" before $ inside loop,
    # verify and discard ")" on exiting loop.


    # Phase 3: Recursion with operator precedence.  Accept an
    # optional operator, end looping when equal or lower
    # precedence operator is seen.
    #
    # Example:
    #
    #	a + b * c + d
    #
    # vm.load('a')
    # text = eval_expression(vm, 'b * c + d', '+')
    #    vm.load('b')
    #    text = eval_expression(vm, 'c + d', '*')
    #       vm.load('c')
    #       return '+ d'
    #    x, y = vm.reduce(2)
    #    vm.shift(x * y) # b * c
    #    return '+ d'
    # x, y = vm.reduce(2)
    # vm.shift(x + y)    # a + (b * c)
    # text = eval_expression('d')
    #    vm.load('d')
    #    return ''
    # x, y = vm.reduce(2)
    # vm.shift(x + y)    # (a + (b * c)) + d
    # return ''

    return text

#---------------------------------------------------------------------#

# Part 2:
#
# Compile by hand the source into a program (list of
# (line_number, statement)).  The Expression and Statement
# subclasses need completing.
#
# Modify run_program() to call stmt.execute() instead of exec_statement.
# Run the hand-compiled program.
#
# Implement compile_program() to produce a Statement from each line
# of the text program.  Copy exec_statement() and eval_expression()
# to create an Expression or Statement to postpone actions on the VM.

class Expression:
    def evaluate(self, vm): XXX

class ConstantExpression(Expression):
    def __init__(self, value):
        self._value = value
    def evaluate(self, vm):
        vm.shift(self._value)
        return

class VariableExpression(Expression):
    def __init__(self, key):
        self._key = key
    def evaluate(self, vm):
        vm.load(self._key)
        return


class Statement:
    def execute(self, vm): XXX

class PrintStatement(Statement):
    def execute(self, vm):
        self._expression.evaluate(vm)
        x, = vm.reduce(1)
        print(x)
        return None

class GotoStatement(Statement):
    def execute(self, vm):
        return self._number

class WhenStatement(Statement):
    def execute(self, vm):
        self._expr_1.evaluate(vm)
        self._expr_2.evaluate(vm)
        x, y = vm.reduce(2)
        return self._number if self._relop(x, y) else None

#---------------------------------------------------------------------#

run_program(program)

#--#
